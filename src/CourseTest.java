import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.After;
import org.junit.Test;

public class CourseTest {

    private Course course;

    @Before
    public void setUp() {
        course = new Course("Math", "Mathematics course", "Passing Math 101");
    }
    

    @Test
    public void testAddNewCourse() {
        assertEquals("Math", course.getTitle());
        assertEquals("Mathematics course", course.getContent());
        assertEquals("Passing Math 101", course.getCriteria());
    }


    @Test
    public void testEnrollNewStudentToCourse_NormalOperation() {
        Student student = new Student("S001", "John Doe");
        course.enrollStudent(student);
        assertTrue(course.getStudents().contains(student));
    }


    @Test
    public void testEnrollNewStudentToCourse_DuplicateEnrollment() {
        Student student = new Student("S001", "John Doe");
        course.enrollStudent(student);

        try {
            course.enrollStudent(student);
            fail("Expected IllegalArgumentException was not thrown");
        } catch (IllegalArgumentException e) {
            assertNotNull(e.getMessage());
            assertEquals("Student is already enrolled in this course.", e.getMessage());
        }
    }

    @After
    public void tearDown() {
        course = null;
    }
}
